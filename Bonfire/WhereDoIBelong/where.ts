/**
 * Return the lowest index at which a value (second argument) should be inserted into a sorted array (first argument).
 * For example, where([1,2,3,4], 1.5) should return 1 because it is greater than 1 (0th index), but less than 2 (1st index).
 * @param  {number[]} arr
 * @param  {number}   num
 * @return {number}
 */
function whereTo(arr: number[], num: number): number {
  let i = 0;
  let current = arr[i];
  while(current < num) {
    current = arr[i++];
  }
  return i-1;
}

where([40, 60], 50);
