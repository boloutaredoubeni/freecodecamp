/**
 * Remove all falsey values from an array.
 * Falsey values in javascript are false, null, 0, "", undefined, and NaN.
 * @param  {any[]} arr
 * @return {any[]}
 */
function bouncer(arr: any[]): any[] {
  return arr.filter( (x: any) => {
    if (!x) return;
    return x;
  });
}

bouncer([7, 'ate', '', false, 9]);
bouncer(['a', 'b', 'c']);
bouncer([false, null, 0]);
