/**
 * Sum all the prime numbers up to and including the provided number.
 * A prime number is defined as having only two divisors, 1 and itself. For example, 2 is a prime number because it's only divisible by 1 and 2. 1 isn't a prime number, because it's only divisible by itself.

 * The provided number may not be a prime.
 * @param  {number} num
 * @return {number}
 */
function sumPrimes(num: number): number {
  if (num === 2) {
    return num;
  }
  /* A function for primality tests */
  const isPrime = (p: number) => {
    // if 1 or even its not prime
    // NOTE: but even numbers should never be passed
    if ( (p===1) || ((p%2)===0) ) {
      return false;
    // after the above condition, all below 4 || 9 are prime
    } else if ((p < 4) || (p < 9)) {
      return true;
    } else if ((p%3)===0) {
      // if it is divisible by 3 it is not prime
      return false;
    }

    // the seive
    const r = Math.floor(Math.sqrt(p));
    for ( let f = 5; f <= r; f += 6 ) {
      if ((p%f)===0 || (p%(f+2)===0)) {
        return false;
      }
    }
    return true;
  }

  let sum = 2;
  for (let n = 3; n <= num; n += 2) {
    if (isPrime(n)) {
      sum += n;
    }
  }

  return sum;
}


sumPrimes(10);
sumPrimes(977);
