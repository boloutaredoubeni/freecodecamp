/**
 * Return the remaining elements of an array after chopping off n elements from the head.
 * @param  {any[]}  arr
 * @param  {number} howMany
 * @return {any[]}         
 */
function slasher(arr: any[], howMany: number): any[] {
  return arr.slice(howMany, arr.length);
}
