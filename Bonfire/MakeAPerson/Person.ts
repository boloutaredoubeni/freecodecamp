
class Person {
  private firstname: string;
  private lastname: string;

  /**
   * Creates the Person object
   * @param  {string} privatefullname
   * @return {Person}
   */
  constructor(private fullname: string) {
    this.firstname = fullname.split(' ')[0];
    this.lastname = fullname.split(' ')[1];
  }

  getFullName(): string {
    return this.fullname;
  }

  setFullName(newName: string) {
    this.fullname = newName;
  }

  getFirstName(): string {
    return this.firstname;
  }

  setFirstName(newName: string) {
    this.firstname = newName;
  }

  getLastName(): string {
    return this.lastname;
  }

  setLastName(newName: string) {
    this.lastname = newName;
  }
}

var bob = new Person('Bob Ross');
