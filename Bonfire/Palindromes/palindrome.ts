/**
 * Return true if the given string is a palindrome. Otherwise, return false.
 * A palindrome is a word or sentence that's spelled the same way both forward and backward, ignoring punctuation, case, and spacing.
 * You'll need to remove punctuation and turn everything lower case in order to check for palindromes.
 * We'll pass strings with varying formats, such as "racecar", "RaceCar", and "race CAR" among others.
 * @param  {string}  str
 * @return {boolean}
 */
function palindrome(str: string): boolean {
  // remove all non alphabetic chars
  let modStr = str.replace(/[^\w]/g, '').toLowerCase();
  // make a reversed copy of the modified string
  let reversedStr = modStr.split('').reverse().join('');
  // return the result of the comparison
  return modStr === reversedStr;
}

palindrome("eye");
palindrome("race car");
palindrome("not a palindrome");
palindrome("A man, a plan, a canal. Panama");
palindrome("never odd or even");
palindrome("nope");
