/**
 * Compare two arrays and return a new array with any items not found in both of the original arrays.
 * @param  {any[]} arr1
 * @param  {any[]} arr2
 * @return {any[]}
 */
function diff(arr1: any[], arr2: any[]): any[] {
  let diffArray = [];

  arr1.forEach( (x1) => {
    if ( arr2.indexOf(x1) === -1 ) {
      diffArray.push(x1);
    }
  });

  arr2.forEach( (x1) => {
    if (arr1.indexOf(x1) === -1) {
      diffArray.push(x1);
    }
  });

  return diffArray;
}

diff([1, 2, 3, 5], [1, 2, 3, 4, 5]);
diff(['diorite', 'andesite', 'grass', 'dirt', 'pink wool', 'dead shrub'], ['diorite', 'andesite', 'grass', 'dirt', 'dead shrub']);
diff(['andesite', 'grass', 'dirt', 'pink wool', 'dead shrub'], ['diorite', 'andesite', 'grass', 'dirt', 'dead shrub']);
diff(['andesite', 'grass', 'dirt', 'dead shrub'], ['andesite', 'grass', 'dirt', 'dead shrub']);
diff([1, 2, 3, 5], [1, 2, 3, 4, 5]);
diff([1, 'calf', 3, 'piglet'], [1, 'calf', 3, 4]);
diff([], ['snuffleupagus', 'cookie monster', 'elmo']);
