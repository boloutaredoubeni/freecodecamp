/**
 * Convert a string to spinal case. Spinal case is all-lowercase-words-joined-by-dashes.
 * @param  {string} str
 * @return {string}
 */
function spinalCase(str: string): string {
  return str.replace(/((?:[a-z])[A-Z]|_|\s)/g, function (val) {
    if (val.length === 2) {
      return val.substring(0, 1) + '-' + val.substring(1, val.length);
    }
    return '-';
  }).toLowerCase();
}

spinalCase('This Is Spinal Tap');
spinalCase('thisIsSpinalTap');
spinalCase('The_Andy_Griffith_Show');
spinalCase('Teletubbies say Eh-oh');
