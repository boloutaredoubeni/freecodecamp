interface SatelliteOrbital {
  name: string,
  orbitalPeriod: number
}

interface SatelliteAlt {
  name: string,
  avgAlt
}

function orbitalPeriod(arr: SatelliteAlt[]): SatelliteOrbital[] {
  const GM = 398600.4418;
  const earthRadius = 6367.4447;

  const period = avgAlt => {
    return Math.round(
      2*Math.PI*Math.sqrt(
        Math.pow(earthRadius+avgAlt, 3) / GM
      )
    );
  }
  return arr.map((x) =>{
    return {
      name: x.name,
      orbitalPeriod: period(x.avgAlt),
    }
  });
}

orbitalPeriod([{name : "sputkin", avgAlt : 35873.5553}]);
orbitalPeriod([{name: "iss", avgAlt: 413.6}, {name: "hubble", avgAlt: 556.7}, {name: "moon", avgAlt: 378632.553}]);
