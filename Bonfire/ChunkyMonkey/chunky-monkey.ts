/**
 * Write a function that splits an array (first argument) into groups the length of size (second argument) and returns them as a multidimensional array.
 * @param  {any[]}  arr
 * @param  {number} size
 * @return {any[]}
 */
function chunk(arr: any[], size: number): any[] {
  let chunkedArray = [];
  for ( let i = 0; i < arr.length; i += size ) {
    chunkedArray.push(arr.slice(i, i+size));
  }
  return chunkedArray;
}

chunk(['a', 'b', 'c', 'd'], 2);
chunk([0, 1, 2, 3, 4, 5], 3);
chunk([0, 1, 2, 3, 4, 5], 2);
chunk([0, 1, 2, 3, 4, 5], 4);
