/**
 * Create a function that looks through an array (first argument) and returns the first element in the array that passes a truth test (second argument).
 * @param  {any[]}    arr
 * @param  {Function} func
 * @return {any}
 */
function find(arr: any[], func: Function): any {
  return arr.filter(<any>func)[0];
}

find([1, 3, 5, 8, 9, 10], function(num) { return num % 2 === 0; });
find([1, 3, 5, 9], function(num) { return num % 2 === 0; });
