function sym(...args: any[]): any[] {
  // a helper function to find the complement
  const complement = (a: any[], b: any[]): any[] => {
    return a.filter( e => {
      return b.indexOf(e) === -1;
    });
  }
  
  return args
    // return the symetric complement
    .reduce( (a: any[], b: any[]): any[] => {
      return complement(a, b).concat(complement(b, a));
    })
    // find all unique elements
    .reduce( (xs, t) => {
      if ( xs.indexOf(t) < 0 ) {
        xs.push(t);
      }
      return xs;
    }, []);
}

sym([1, 2, 3], [5, 2, 1, 4]);
sym([1, 2, 5], [2, 3, 5], [3, 4, 5]);
sym([1, 1, 2, 5], [2, 2, 3, 5], [3, 4, 5, 5]);
sym([1, 1]);
