/**
 * Check if a string (first argument) ends with the given target string (second argument).
 * @param  {string}  str    The string to be checked
 * @param  {string}  target The string that 'str' should end with
 * @return {boolean}
 */
function end(str: string, target: string): boolean {
  return target === str.substr(-target.length);
}

end('Bastian', 'n');
end('Connor', 'n');
end('Walking on water and developing software from a specification are easy if both are frozen.', 'specification');
end('He has to give me a new name', 'name');
end('If you want to save our world, you must hurry. We dont know how much longer we can withstand the nothing', 'mountain');
