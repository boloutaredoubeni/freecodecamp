/**
 * Flatten a nested array. You must account for varying levels of nesting.
 * @param  {any} arr
 * @return {any}
 */
function steamroller(arr: any): any {
  return arr.reduce((a, b) => {
    // concat and recurse
    return a.concat(Array.isArray(b) ? steamroller(b) : b);
  }, []);
}

steamroller([[['a']], [['b']]]);
steamroller([1, [2], [3, [[4]]]]);
steamroller([1, [], [3, [[4]]]]);
