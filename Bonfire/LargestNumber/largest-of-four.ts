/**
 * Return an array consisting of the largest number from each provided sub-array. For simplicity, the provided array will contain exactly 4 sub-arrays.
 * Remember, you can iterate through an array with a simple for loop, and access each member with array syntax arr[i]
 * @param  {number[][]} arr
 * @return {number[]}
 */
function largestOfFour(arr: number[][]): number[] {
  // this function should work for arrays of any given size
  return arr.map( (ys: number[]) => {
    // map a function that finds the max of every array
    return ys.reduce( (x1: number, x2: number) => {
      // reduce the array until the max is left
      return x1 < x2 ? x2 : x1
    });
  });
}

largestOfFour([[4, 5, 1, 3], [13, 27, 18, 26], [32, 35, 37, 39], [1000, 1001, 857, 1]]);
largestOfFour([[4, 9, 1, 3], [13, 35, 18, 26], [32, 35, 97, 39], [1000000, 1001, 857, 1]]);
