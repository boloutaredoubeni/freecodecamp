/**
 * Check if the predicate (second argument) returns truthy (defined) for all elements of a collection (first argument).

For this, check to see if the property defined in the second argument is present on every element of the collection.

Remember, you can access object properties through either dot notation or [] notation.
 * @param  {Object[]} collection
 * @param  {any}      pre        Object or a string
 * @return {boolean}
 */
function every(collection: Object[], pre: any): boolean {

  var out = collection.filter( (x: Object) => {
    const ppty = Object.getOwnPropertyNames(x);
    if ( typeof pre === 'object' ) {
      const attr = Object.getOwnPropertyNames(pre)[0];
      return x[attr] === pre[attr];
    }
    return ppty.indexOf(pre) >= 0;
  });
  
  return out.length === collection.length;
}

every([{'user': 'Tinky-Winky', 'sex': 'male'}, {'user': 'Dipsy', 'sex': 'male'}, {'user': 'Laa-Laa', 'sex': 'female'}, {'user': 'Po', 'sex': 'female'}], 'sex');
every([{'user': 'Tinky-Winky', 'sex': 'male'}, {'user': 'Dipsy', 'sex': 'male'}, {'user': 'Laa-Laa', 'sex': 'female'}, {'user': 'Po', 'sex': 'female'}], {'sex': 'female' });
