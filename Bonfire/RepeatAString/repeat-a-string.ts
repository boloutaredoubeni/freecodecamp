/**
 * Repeat a given string (first argument) n times (second argument). Return an empty string if n is a negative number.
 * @param  {string} str the string that will be repeated
 * @param  {number} num Number oftimes to be repeated
 * @return {string}     The repeated string
 */
function repeat(str: string, num: number): string {
  return num >= 0 ? (new Array(num+1)).join(str) : '' ;
}

repeat('abc', 3);
repeat('*', 3);
repeat('abc', -2);
