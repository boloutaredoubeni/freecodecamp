function add(...args) {
  for ( let x in args ) {
    if ( typeof args[x] !== 'number' ) {
      return undefined;

    }
  }


  if ( args.length < 2 ){
    return n => {
      return add( n, args[0] );
    }
  }
  return args.reduce( (a,b) => { return a + b});
}

add(2, 3);
add(2)(3);
/*add('http://bit.ly/IqT6zt');*/
add(2, '3');
add(2)([3]);
