/**
 * Drop the elements of an array (first argument), starting from the front, until the predicate (second argument) returns true.
 * @param  {any[]}    arr
 * @param  {Function} func
 * @return {any[]}
 */
function drop(arr: any[], func: Function): any[] {
  return arr.filter(<any>func);
}

drop([1, 2, 3], function(n) {return n < 3; });
drop([1, 2, 3], function(n) {return n > 0; });
drop([1, 2, 3, 4], function(n) {return n > 5; });
