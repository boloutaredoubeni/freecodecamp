enum InventoryEntry {
    kStock,
    kItem
}


function inventory(arr1: any[][], arr2: any[][]): any[] {
    let newInventory = [];
        
    arr2.forEach( newInventoryItem => {
        arr1.forEach( oldInventoryItem => {
            if ( newInventoryItem[InventoryEntry.kItem] === oldInventoryItem[InventoryEntry.kItem ]) {
                oldInventoryItem[InventoryEntry.kStock] +=  newInventoryItem[InventoryEntry.kStock];
            }
        });
        
        if ( newInventory.indexOf( newInventoryItem) < 0 ) {
            newInventory.push( newInventoryItem );
        }
    });
    
    return newInventory;
}

// Example inventory lists
var curInv = [
    [21, 'Bowling Ball'],
    [2, 'Dirty Sock'],
    [1, 'Hair Pin'],
    [5, 'Microphone']
];

var newInv = [
    [2, 'Hair Pin'],
    [3, 'Half-Eaten Apple'],
    [67, 'Bowling Ball'],
    [7, 'Toothpaste']
];

inventory(curInv, newInv);
inventory([[21, 'Bowling Ball'], [2, 'Dirty Sock'], [1, 'Hair Pin'], [5, 'Microphone']], 
    [[2, 'Hair Pin'], [3, 'Half-Eaten Apple'], [67, 'Bowling Ball'], [7, 'Toothpaste']]);
inventory([[21, 'Bowling Ball'], [2, 'Dirty Sock'], [1, 'Hair Pin'], [5, 'Microphone']], []);
inventory([], [[2, 'Hair Pin'], [3, 'Half-Eaten Apple'], [67, 'Bowling Ball'], [7, 'Toothpaste']]);
inventory([[0, 'Bowling Ball'], [0, 'Dirty Sock'], [0, 'Hair Pin'], [0, 'Microphone']], [[1, 'Hair Pin'], 
    [1, 'Half-Eaten Apple'], [1, 'Bowling Ball'], [1, 'Toothpaste']]);
