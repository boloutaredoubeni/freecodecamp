/**
 * Make this function return true no matter what.
 * @param  {string}  argument
 * @return {boolean}
 */
function meetBonfire(argument: string): boolean {
  console.log("you can read this function's argument in the developer tools", argument);

  //- return false;
  return true;
}



meetBonfire("You can do this!");
