/**
 * Return the provided string with the first letter of each word capitalized.
 * For the purpose of this exercise, you should also capitalize connecting words like 'the' and 'of'.
 * @param  {string} str [description]
 * @return {string}     [description]
 */
function titleCase(str: string): string {
  // map a title capitalization function to every word
  return str.split(' ').map((word: string) => {
    // the first letter is always capitalized
    return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    // ... make sure the remainder of the word is in lowercase
  }).join(' ');
}

titleCase("I'm a little tea pot");
