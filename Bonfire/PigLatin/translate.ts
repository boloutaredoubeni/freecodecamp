/**
 * Translate the provided string to pig latin.
 * Pig Latin takes the first consonant (or consonant cluster) of an English word, moves it to the end of the word and suffixes an "ay".
 *
 * If a word begins with a vowel you just add "way" to the end.
 * @param  {string} str [description]
 * @return {string}     [description]
 */
function translate(str: string): string {
  var translation = '';
  var firstVowelIdx = str.search(/[aeiouAEIOU]/g);
  if ( firstVowelIdx === 0 ) {
    translation = str + 'way';
  } else {
    const cluster = str.slice(0, firstVowelIdx);
    translation = str.slice(firstVowelIdx) + cluster + 'ay';
  }

  return translation;
}

translate("consonant");
translate("paragraphs");
translate("glove");
translate("algorithm");
translate("eight");
