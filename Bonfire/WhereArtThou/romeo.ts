/**
 * Make a function that looks through a list (first argument) and returns an array of all objects that have equivalent property values (second argument)
 * @param  {Object[]} collection
 * @param  {Object}   source
 * @return {Object[]}               [description]
 */
function where(collection: Object[], source: Object): Object[] {
  let arr = [];
  const keys = Object.keys(source);
  collection.forEach( (k: Object) => {
    if ((k.hasOwnProperty(keys[0])) && (k[keys[0]] === source[keys[0]])) {
      arr.push(k);
    }
  });
  return arr;
}

where([{ first: 'Romeo', last: 'Montague' }, { first: 'Mercutio', last: null }, { first: 'Tybalt', last: 'Capulet' }], { last: 'Capulet' });
where([{ 'a': 1 }, { 'a': 1 }, { 'a': 1, 'b': 2 }], { 'a': 1 });
