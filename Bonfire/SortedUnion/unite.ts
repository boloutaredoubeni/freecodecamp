/**
 * Write a function that takes two or more arrays and returns a new array of unique values in the order of the original provided arrays.
 * In other words, all values present from all arrays should be included in their original order, but with no duplicates in the final array.
 * The unique numbers should be sorted by their original order, but the final array should not be sorted in numerical order.
 * @param  {[type]} arr1
 * @param  {[type]} arr2
 * @param  {[type]} arr3
 * @param  {[type]} ...args
 * @return {any[]}
 */
function unite(arr1, arr2, arr3, ...args): any[] {
  const lesArgs = Array.prototype.slice.call(arguments);
  let oddBalls = [];
  // TODO: use reduce instead
  lesArgs.forEach((y) => {
    y.forEach((x) => {
      if (oddBalls.indexOf(x) === -1)
        oddBalls.push(x);
      });
    });

  return oddBalls;
}

unite([1, 2, 3], [5, 2, 1, 4], [2, 1]);
unite([1, 3, 2], [1, [5]], [2, [4]]);
