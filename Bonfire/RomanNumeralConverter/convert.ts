/**
 * Convert the given number into a roman numeral.
 * All roman numerals answers should be provided in upper-case.
 * @param  {number} num
 * @return {string}
 */
function convert(num: number): string {
  let romanNumeral = '';
  const numerals = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
  const roman = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

  numerals.forEach((x, i) => {
    while (num >= x) {
      romanNumeral += roman[i];
      num -= x;
    }
  });

  return romanNumeral;
}

convert(12);
convert(5);
convert(9);
convert(29);
convert(16);
