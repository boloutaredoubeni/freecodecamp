/**
 * Return the sum of all odd Fibonacci numbers up to and including the passed number if it is a Fibonacci number.
 * The first few numbers of the Fibonacci sequence are 1, 1, 2, 3, 5 and 8, and each subsequent number is the sum of the previous two numbers.
 * As an example, passing 4 to the function should return 5 because all the odd Fibonacci numbers under 4 are 1, 1, and 3.
 * @param  {number} num
 * @return {number}
 */
function sumFibs(num: number): number {
  let ans = 1;
  let c = 0;
  for ( let a=0, b=1; b <= num; c=a+b, a=b, b=c ) {
    if (c%2 === 1) {
      ans += c;
    }
  }
  return ans;
}

sumFibs(4);
