/**
 * Perform a search and replace on the sentence using the arguments provided and return the new sentence.
 * First argument is the sentence to perform the search and replace on.
 * Second argument is the word that you will be replacing (before).
 * Third argument is what you will be replacing the second argument with (after).
 * NOTE: Preserve the case of the original word when you are replacing it. For example if you mean to replace the word 'Book' with the word 'dog', it should be replaced as 'Dog'
 * @param  {string} str
 * @param  {string} before
 * @param  {string} after
 * @return {string}
 */
function replace(str: string, before: string, after: string): string {
  // Compare the before vs. after and match the cases
  const matchedAfter = ((prev: string, next: string): string => {
    var firstLetterCode = prev.charCodeAt(0);
    // check if lowercase
    if ((firstLetterCode >= 97) && ( firstLetterCode <= 122)) {
      return next.toLowerCase();
    }
    // check if uppercase
    if ((firstLetterCode >= 65) && ( firstLetterCode <= 90)) {
      return next.charAt(0).toUpperCase() + next.substr(1, next.length);
    }
    // NOTE: This should never be reached
    return next;

  })(before, after);

  return str.replace(before, matchedAfter);
}

replace("A quick brown fox jumped over the lazy dog", "jumped", "leaped");
replace("He is Sleeping on the couch", "Sleeping", "sitting");
replace("This has a spellngi error", "spellngi", "spelling");
replace("His name is Tom", "Tom", "john");
replace("Let us get back to more Coding", "Coding", "bonfires");
