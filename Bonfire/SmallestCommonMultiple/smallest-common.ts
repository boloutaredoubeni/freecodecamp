/**
 * Find the smallest number that is evenly divisible by all numbers in the provided range.
 * The range will be an array of two numbers that will not necessarily be in numerical order.
 * @param  {number[]} arr
 * @return {number}
 */
function smallestCommons(arr: number[]): number {
  const xs = arr.sort();
  const min =  xs[0], max = xs[1];

  // Implement a recursive gcd function
  function gcd(a, b): number {
    return b ? gcd(b, a%b): a;
  }

  // Implement lcm function
  const lcm = (a, b) => {
    return (a*b) / gcd(a,b);
  }


  let ans = 1;
  for (let i = min; i <= max; ++i)
    ans = lcm(ans, i);

  return ans;
}

smallestCommons([1,5]);
smallestCommons([5,1]);
smallestCommons([1,13]);
