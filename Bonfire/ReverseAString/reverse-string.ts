/**
 * Reverse the provided string.
 * @param  {string} str Any string object
 * @return {string}     A string that has been reversed
 */
function reverseString(str: string): string {
  return str.split('').reverse().join('');
}

reverseString('hello');
reverseString('Howdy');
reverseString('Greetings from Earth');
