/**
 * Convert the characters "&", "<", ">", '"' (double quote), and "'" (apostrophe), in a string to their corresponding HTML entities.
 * @param  {string} str
 * @return {string}
 */


 const enum ConversionState {
   REGEX, ENTITY,
 }

function convertHTML(str: string): string {
  const CONVERSIONS = [
    [/&/g, /</g, />/g, /"/g, /'/g],
    ['&amp;', '&lt;', '&gt;', '&quot;', '&apos;']
  ];

  return str
    .replace(<RegExp>CONVERSIONS[ConversionState.REGEX][0],
             <string>CONVERSIONS[ConversionState.ENTITY][0])
    .replace(<RegExp>CONVERSIONS[ConversionState.REGEX][1], 
             <string>CONVERSIONS[ConversionState.ENTITY][1])
    .replace(<RegExp>CONVERSIONS[ConversionState.REGEX][2],
             <string>CONVERSIONS[ConversionState.ENTITY][2])
    .replace(<RegExp>CONVERSIONS[ConversionState.REGEX][3],
             <string>CONVERSIONS[ConversionState.ENTITY][3])
    .replace(<RegExp>CONVERSIONS[ConversionState.REGEX][4],
             <string>CONVERSIONS[ConversionState.ENTITY][4]);
}

convertHTML('Dolce & Gabbana');
convertHTML('Hamburgers < Pizza < Tacos')
convertHTML('Sixty > twelve');
convertHTML('Stuff in "quotation marks"');
convertHTML("Shindler's List");
convertHTML('<>');
convertHTML('abc');
