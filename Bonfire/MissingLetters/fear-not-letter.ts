 /**
  * Find the missing letter in the passed letter range and return it.
  * If all letters are present in the range, return undefined.
  * @param  {[type]} str: string        [description]
  * @return {[type]}      [description]
  */
function fearNotLetter(str: string): string {
  var i = 0;
  var code = str.charCodeAt(i);
  var ch = code;
  while (code === ch) {
    if (i === str.length-1) return undefined;
    code = str.charCodeAt(++i);
    ch++;
  }
  return String.fromCharCode(ch);
}
