/**
 * You will be provided with an initial array (the first argument in the destroyer function), followed by one or more arguments. Remove all elements from the initial array that are of the same value as these arguments.
 * @param  {any[]} arr
 * @return {any[]}
 */
function destroyer(arr: any[], ...args): any[] {
  let markedForDeletion = [];
  for ( let i = 1; i < arguments.length; ++i ) {
    markedForDeletion.push(arguments[i]);
  }

  return arr.filter(function(x: any): any {
    return markedForDeletion.indexOf(x) < 0;
  });
}

destroyer([1, 2, 3, 1, 2, 3], 2, 3);
destroyer([1, 2, 3, 5, 1, 2, 3], 2, 3);
