/**
 * Check if a value is classified as a boolean primitive. Return true or false.
 * Boolean primitives are true and false.
 * @param  {Boolean} bool
 * @return {boolean}
 */
function boo(bool: any): boolean {
  return bool === true || bool === false;
}

boo(null);
boo(true);
boo(false);
boo([1, 2, 3]);
boo([].slice);
boo({ 'a': 1 });
boo(1);
boo(NaN);
boo('a');
