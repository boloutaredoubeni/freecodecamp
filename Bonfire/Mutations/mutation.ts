/**
 * Return true if the string in the first element of the array contains all of the letters of the string in the second element of the array.
 * For example, ['hello', 'Hello'], should return true because all of the letters in the second string are present in the first, ignoring case.
 * The arguments ['hello', 'hey'] should return false because the string 'hello' does not contain a 'y'.
 * Lastly, ['Alien', 'line'], should return true because all of the letters in 'line' are present in 'Alien'.
 * @param  {string[]} arr
 * @return {boolean}      s
 */
function mutation(arr: string[]): boolean {
  let flag = true;

  // make the two words the same case
  const tmp = arr.map((word: string) => { return word.toLowerCase(); });
  // make the second word into an array
  const secondWord = tmp[1].split('');

  // check if letter in the second word is contained in the first
  secondWord.forEach((letter: string) => {
    // return false immediately if a letter is not found
    if (tmp[0].indexOf(letter) === -1) flag = false;
  });

  return flag;  // return otherwise
}

mutation(['hello', 'hey']);
mutation(['hello', 'Hello']);
mutation(['zyxwvutsrqponmlkjihgfedcba', 'qrstu']);
mutation(['Mary', 'Army']);
mutation(['Mary', 'Aarmy']);
mutation(['Alien', 'line']);
mutation(['floor', 'for']);
