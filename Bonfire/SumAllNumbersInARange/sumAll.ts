/**
 * We'll pass you an array of two numbers. Return the sum of those two numbers and all numbers between them.
 * The lowest number will not always come first.
 * @param  {number[]} arr
 * @return {number}
 */
function sumAll(arr: number[]): number {
  var listNums = [];
  const min = Math.min(arr[0], arr[1]);
  const max = Math.max(arr[0], arr[1]);
  for ( let i = min; i <= max; ++i ) {
    listNums.push(i);
  }

  return listNums.reduce((x1: number, x2: number) => {
    return x1 + x2;
  }, 0);
}

sumAll([1, 4]);
