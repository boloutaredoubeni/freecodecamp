/**
 * Return the length of the longest word in the provided sentence.
 * Your response should be a number.
 * @param  {string} str
 * @return {number}
 */
function findLongestWord(str: string): number {
  return str
    // move every word into an array
    .split(' ')
    // make an array of the respective lengths of the words
    .map((word: string) => { return word.length; })
    // reduce the array until the max value is found
    .reduce((a: number, b: number) => { return a < b ? b : a; });
}

findLongestWord('The quick brown fox jumped over the lazy dog');
findLongestWord('May the force be with you');
findLongestWord('Google do a barrel roll');
findLongestWord('What is the average airspeed velocity of an unladen swallow');
