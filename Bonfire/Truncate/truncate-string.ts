function truncate(str: string, num: number): string {
  const ellipsis = '...';
  return str.length>num ? str.slice(0, num-ellipsis.length).concat(ellipsis) : str;
}

truncate('A-tisket a-tasket A green and yellow basket', 11);
truncate('A-tisket a-tasket A green and yellow basket', 'A-tisket a-tasket A green and yellow basket'.length);
truncate('A-tisket a-tasket A green and yellow basket', 'A-tisket a-tasket A green and yellow basket'.length + 2);
