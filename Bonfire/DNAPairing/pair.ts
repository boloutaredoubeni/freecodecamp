/**
 * The DNA strand is missing the pairing element. Match each character with the missing element and return the results as a 2d array.
 * Base pairs are a pair of AT and CG. Match the missing element to the provided character.
 * Return the provided character as the first element in each array.
 * @param  {any}      str [description]
 * @return {string[]}     [description]
 */
function pair(str: any): string[][] {
  var pairing = [];
  for ( let i in str.split('') ) {
    switch(str[i]) {
     case 'A':
       pairing.push(['A', 'T']);
       break;
     case 'T':
       pairing.push(['T', 'A']);
       break;
     case 'C':
       pairing.push(['C', 'G']);
       break;
     case 'G':
       pairing.push(['G', 'C']);
       break;
     }
  }
  return pairing;
}

pair("GCG");
pair("ATCGA");
pair("TTGAG");
pair("CTCTA");
